package ic.base.vectors.int64vec2


import ic.base.primitives.int64.Int64


data class Int64Vector2 (
	val x : Int64,
	val y : Int64
)