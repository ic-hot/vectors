package ic.base.vectors.int64vec2


import ic.base.primitives.int64.Int64


@Suppress("NOTHING_TO_INLINE")
inline fun vec (x: Int64, y: Int64) = Int64Vector2(x, y)