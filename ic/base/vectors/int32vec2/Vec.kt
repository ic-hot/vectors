package ic.base.vectors.int32vec2


import ic.base.primitives.int32.Int32


@Suppress("NOTHING_TO_INLINE")
inline fun vec (x: Int32, y: Int32) = Int32Vector2(x, y)