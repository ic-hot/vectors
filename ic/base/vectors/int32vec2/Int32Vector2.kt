package ic.base.vectors.int32vec2


import ic.base.primitives.int32.Int32


data class Int32Vector2 (
	val x : Int32,
	val y : Int32
)